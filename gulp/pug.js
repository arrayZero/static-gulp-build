const gulp = require('gulp');
const pug = require('gulp-pug');
const data = require('gulp-data');
var path = require('path');

gulp.task('pug', function () {
	return gulp
		.src('./build_assets/pug/pages/**/*.pug')
		.pipe(data(function (file) {
			fileName = path.basename(file.path).slice(0, -4).replace(/-/g, '_').toLowerCase();
			filePath = path.relative('./build_assets/pug/pages/', file.path);
			fileDir = '';

			if (filePath.indexOf("/") > 0) {
				fileDir = filePath.slice(0, filePath.indexOf("/"));
			}
			canonicalName = path.basename(file.path).slice(0, -4);

			// rename some stuff:
			if (fileName === "index") {
				fileName = 'homepage';
			} else if (fileName === "404") {
				fileName = 'four0four';
			}

			console.log(filePath);

			return {
				'fileName': fileName,
				'fileDir': fileDir,
				'canonicalName': canonicalName
			};
		}))
		.pipe(pug({
			pretty: false,
			doctype: 'html'
		}))
		.pipe(gulp.dest('public_html/'))
		.pipe(browserSync.stream());
});