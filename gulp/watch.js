const gulp = require('gulp');

gulp.task('watch', function () {
	browserSync.init({
			server: {
				baseDir: "./public_html",
				serveStaticOptions: {
					extensions: ['html']
				}
			}
		},
		function (err, bs) {
			bs.addMiddleware("*", function (req, res) {
				res.writeHead(302, {
					location: "404.html"
				});
				res.end("Redirecting!");
			});
		});
	// generate
	gulp.watch('./build_assets/scss/**/*.scss', gulp.series('css'));
	gulp.watch('./build_assets/js/**/*.js', gulp.series('JavaScript'));
	gulp.watch('./build_assets/pug/**/*.pug', gulp.series('pug'));
	// copy
	gulp.watch('./assets/images/**/*', gulp.series('images'));
	gulp.watch('./assets/pdfs/**/*', gulp.series('pdfs'));
	gulp.watch('./assets/videos/**/*', gulp.series('videos'));
	gulp.watch('./assets/fonts/**/*', gulp.series('fonts'));
});