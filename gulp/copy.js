const gulp = require('gulp');


/*** COPY TASK
Function: copyAssets
Purpose: Copy any assets that do not need processing or manipulation.
Task has been split into sepparate tasks for easy modification.
Parameters: gulp.series
Returns:
TODO:
***/

gulp.task('fonts', function (done) {
	gulp.src(['./node_modules/font-awesome/fonts/**/*', './assets/fonts/**/*'])
		.pipe(gulp.dest('./public_html/fonts'));
	done();
});

gulp.task('pdfs', function (done) {
	gulp.src(['./assets/pdfs/*'])
		.pipe(gulp.dest('./public_html/pdfs'));
	done();
});

gulp.task('images', function (done) {
	gulp.src(['./assets/images/**/*'])
		.pipe(gulp.dest('./public_html/images'));
	done();
});

gulp.task('videos', function (done) {
	gulp.src(['./assets/videos/**/*'])
		.pipe(gulp.dest('./public_html/videos'));
	done();
});

gulp.task('favicons', function (done) {
	gulp.src('./assets/favicons/**/*')
		.pipe(gulp.dest('./public_html/'));
	done();
});

gulp.task('SEO', function (done) {
	gulp.src('./assets/seo/**/*')
		.pipe(gulp.dest('./public_html/'));
	done();
});

gulp.task('copyAssets', gulp.series('favicons', 'SEO', 'fonts', 'images', 'pdfs', 'videos'));