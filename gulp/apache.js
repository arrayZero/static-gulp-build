const gulp = require('gulp');
const gulpif = require('gulp-if');
const argv = require('minimist')(process.argv.slice(2));
const prod = argv.env === 'prod';

const prodApache = ['./apache/prod/**/*', './apache/prod/**/.*', './apache/prod/.**/*', './apache/prod/.**/.*', '!./apache/prod/.gitkeep'];
const devApache = ['./apache/dev/**/*', './apache/dev/**/.*', './apache/dev/.**/*', './apache/dev/.**/.*', '!./apache/dev/.gitkeep'];


/*** COPY TASK
Function: moves apache configs base on env
Purpose: Defaults to Dev env.
Parameters: gulp --env prod
Returns:
TODO:
***/

gulp.task('apache', function (done) {
	gulp.src(gulpif(prod, prodApache, devApache))
		.pipe(gulp.dest('./public_html/'));
	done();
});