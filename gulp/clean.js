/*** CLEAN TASK
Function: clean
Purpose: del running in a promise, delete completes befor running anything else in series
Parameters:
Returns:
TODO:
***/
const gulp = require('gulp');
const del = require('del');

gulp.task('clean', function () {
	return new Promise(function (resolve, reject) {
		del.sync(['public_html/**/*', '!public_html/.elasticbeanstalk', '!public_html/.elasticbeanstalk/**/*'], {
			dot: true
		});
		resolve();
	});
});