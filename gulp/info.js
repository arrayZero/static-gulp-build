/*** INFO TASK
Function: info
Purpose: Upfront info about the gulp workflow that is running, like what env the task is running under or what tasks are available.
Parameters: gulp.series
Returns:
TODO:
***/
const gulp = require('gulp');
const c = require('ansi-colors');
const argv = require('minimist')(process.argv.slice(2));

gulp.task('info', function (done) {
	console.log(c.bgRed.bold(`⚠️  ENV SET AS: ${c.yellowBright(argv.env != 'prod' ? 'DEV' : 'PROD')} ⚠️ `));
	console.log(c.bold(`⚠️  Tasks available: ⚠️`));
	console.log(c.bold(`📝  Gulp --env prod; ${c.yellowBright('For no sourcemaps.')}`));
	console.log(c.bold(`📝  Gulp; ${c.yellowBright('For preview/staging build.')}`));
	console.log(c.bold(`📝  Gulp dev; ${c.yellowBright('For clean dev build & watch.')}`));
	done();
});