const gulp = require('gulp');
const sass = require('gulp-sass');
const cssnano = require('gulp-cssnano');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const rename = require('gulp-rename');
const gulpif = require('gulp-if');
const argv = require('minimist')(process.argv.slice(2));
const prod = argv.env !== 'prod';


gulp.task('css', function () {
	return gulp
		.src('./build_assets/scss/main.scss')
		.pipe(gulpif(prod, sourcemaps.init()))
		.pipe(sass().on('error', sass.logError))
		.pipe(
			autoprefixer({
				browsers: ['last 2 versions'],
				cascade: false
			})
		)
		.pipe(cssnano({
			zindex: false
		}))
		.pipe(gulpif(prod, sourcemaps.write()))
		.pipe(rename({
			basename: 'styles'
		}))
		.pipe(gulp.dest('./public_html/css/'))
		.pipe(browserSync.stream());
});