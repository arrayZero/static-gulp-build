const gulp = require('gulp');
const babel = require('gulp-babel');
const eslint = require('gulp-eslint');
const modernizr = require('gulp-modernizr');
const sourcemaps = require('gulp-sourcemaps');
const concat = require('gulp-concat');
const babelPreset = require('babel-preset-env');
const uglify = require('gulp-uglify');
const gulpif = require('gulp-if');
const argv = require('minimist')(process.argv.slice(2));
const prod = argv.env !== 'prod';

// add and remove libraries/files as needed
const js_tools = ['./build_assets/js/tools/tools.js', // catch all for tools referenced by multiple scripts
	'./build_assets/js/tools/scrollto.js', // animated scroll functionality
	'./build_assets/js/tools/expandCollapse.js', // expand collapse functionality
	'./build_assets/js/tools/modals.js', // modal content functionality
	'./build_assets/js/tools/sticky_isi.js', // sticky is script
	'./build_assets/js/tools/marketo.js', // marketo form reset
	'./build_assets/js/tools/marketoUTM.js' // marketo tracking script
];

const js_libs = [

	// './build_assets/js/vendor/dpi-ppc-tracking-script.js', // old marketo tracking script
	'./node_modules/babel-polyfill/dist/polyfill.js', // pollyfills for older browsers
	'./node_modules/scroll-to-element/build/scrollToElement.js', // ie 11 compatable scroll-to animation
	'./node_modules/animejs/lib/anime.min.js', // animation library
	'./node_modules/emergence.js/dist/emergence.js' // element on screen detection
];

// site custom js
const js_main = ['./build_assets/js/*.js'];

// merge lists
const js_files = js_libs.concat(js_tools, js_main);


gulp.task('modernizr', function () {
	return gulp.src(js_main)
		.pipe(modernizr({
			"minify": true,
			"options": [
				"setClasses",
				"html5printshiv",
				"testProp",
				"fnBind"
			],
			"feature-detects": [
				"css/filters",
				"css/flexbox",
				"css/flexboxlegacy",
				"css/flexboxtweener",
				"css/flexwrap",
				"css/positionsticky",
				"css/scrollbars",
				"css/scrollsnappoints",
				"css/shapes",
				"css/supports",
				"elem/picture",
				"svg",
				"svg/asimg",
				"svg/clippaths",
				"svg/filters",
				"svg/foreignobject",
				"svg/inline",
				"svg/smil",
				"touchevents"
			]
		}))
		.pipe(gulp.dest("./public_html/js/"));
});


gulp.task('lint', function () {
	return gulp.src(js_main)
		.pipe(eslint({
			"parserOptions": {
				"ecmaVersion": 6,
				"sourceType": "module",
				"ecmaFeatures": {
					"jsx": true
				}
			},
			"env": {
				"node": true,
				"es6": true,
				"browser": true
			},
			"extends": "eslint:recommended",
			"rules": {
				"no-console": [1, {
					allow: ['warn', 'error']
				}],
				"block-scoped-var": 1,
				"consistent-return": 2,
				"curly": 2,
				"eqeqeq": [2, "smart"],
				"no-warning-comments": [1, {
					"terms": ["TODO", "FIXME"],
					"location": "start"
				}],
				"no-undef-init": 0,
				"no-undef": 0,
				"no-undefined": 0,
				"no-useless-escape": 0,
				"no-unused-vars": 1,
				"arrow-spacing": [2, {
					"before": true,
					"after": true
				}],
				"array-bracket-spacing": [0, "never"],
				"block-spacing": [1, "always"],
				"brace-style": [1, "1tbs", {
					"allowSingleLine": false
				}],
				"camelcase": 1,
				"comma-spacing": [1, {
					"before": false,
					"after": true
				}],
				"comma-style": [1, "last"],
				"indent": [0, 'tab'],
				"no-inline-comments": 0,
				"no-lonely-if": 1,
				"no-mixed-spaces-and-tabs": 1,
				"no-multiple-empty-lines": 1,
				"no-nested-ternary": 1,
				"no-trailing-spaces": 1,
				"quotes": [1, "single"],
				"space-in-parens": [1, "never"],
			}
		})) // see .eslintrc filein theme root for settings
		.pipe(eslint.format())
		.pipe(eslint.failAfterError());
});


gulp.task('babel', function () {
	return gulp.src(js_files)
		.pipe(gulpif(prod, sourcemaps.init()))
		.pipe(babel({
			'presets': [
				[babelPreset, {
					"targets": {
						"ie": "11"
					}
				}]
			],
			'plugins': [
				"transform-es2015-template-literals",
				"syntax-dynamic-import",
				"transform-es2015-arrow-functions",
				"transform-es2015-parameters",
				["transform-es2015-for-of", {
					"loose": true
				}],
				["transform-es2015-spread", {
					"loose": true
				}],
				["transform-es2015-destructuring", {
					"loose": true
				}],
				["transform-es2015-block-scoping", {
					"throwIfClosureRequired": true
				}]
			],
			'ignore': js_libs, //files to ignore
			'compact': true
		}))
		.pipe(concat('main_min.js'))
		.pipe(uglify())
		.pipe(gulpif(prod, sourcemaps.write()))
		.pipe(gulp.dest('./public_html/js/'))
		.pipe(browserSync.stream());
});


gulp.task('JavaScript', gulp.series('lint', 'babel'));