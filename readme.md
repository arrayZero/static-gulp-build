# Static web site base build

## TODO:
- add browsersync middleware to mimic .htaccess
- add js link file type detection
- update sticky isi functionality


## Basic Dependencies:

* **Pacakage Manager:** NPM
* **Build System:** Gulp
* **Template System:** Pug
* **CSS:** Sass
* **JS:** Babel

## Getting Started:
```
npm install
npm run dev
```
Installs & builds all files and moves all ssets and starts browsersync & watch.

### Structure
Any static content goes in the assets folder. File types such as image**\***, pdf, video & fonts.
**\*Make sure you compress all images using imageOptim before pushing to preview/staging/production.**

####JS:
In ./gulp/javaScript.js add and remove libraries/files from js_tools & js_libs as needed.
*TODO: Add descriptions of libraries/files*


####SCSS:

- Base styles set in base dir
- components dir: repeatable components/design patterns. Add & include page files as needed.
- pages dir: page style violators. Add & include page files as needed.
- 2 themes proivided, enable in main.scss. Any mods or new layouts add & include page files as needed.

#### PUG:

- includes/variables.pug is for storing persistant variables for all pages, such as descriptions page titles etc...