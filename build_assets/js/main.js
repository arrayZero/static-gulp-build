/*
 * ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 * ========================================================================
 */

// Use this variable to set up the common and page specific functions. If you
// rename this variable, you will also need to rename the namespace below.

var FPM = {
	// All pages
	'common': {
		init: function () {
			// initalize site functionality objects
			Modals.init();
			ExternalLinks.init();

			MarketoFormReset.init();
			// DISABLE ISI ON ISI PAGE
			if (window.location.href.indexOf('important-safety-information') < 0) {
				StickyISI.init();
			}
			// looking on every page for expand for demo.
			Expand.init({
				elementGroup: 'expand',
				collapseOnOpen: true,
				openFirst: true
			});
			// looking on every page for expand for demo.
			Expand.init({
				elementGroup: 'solo',
				collapseOnOpen: true,
				openFirst: true
			});

			// NAV FUNCTION INIT
			// for _default layout
			// FPM.common.navigation('hover');
			// for _sidebar layout
			FPM.common.navigation();
		},


		/*
		 * ========================================================================
		 * Navigation collapse & expand functionality
		 * TODO: move to own file
		 * ========================================================================
		 */
		navigation: function (eventTrigger) {
			// MOBILE NAVIGATION:
			var navbarOpen = document.getElementById('navbar-open');
			var navbarClose = document.getElementById('navbar-close');
			var backdrop = document.getElementById('mobile_nav_backdrop');
			var nav = document.getElementById('navigation');
			var submenus = nav.getElementsByClassName('sub');

			navbarOpen.addEventListener('click', function () {
				nav.classList.add('open');
				document.body.classList.add('takeover', 'nav');
			});

			backdrop.addEventListener('click', function () {
				nav.classList.remove('open');
				document.body.classList.remove('takeover', 'nav');
			});

			navbarClose.addEventListener('click', function () {
				nav.classList.remove('open');
				document.body.classList.remove('takeover', 'nav');
			});

			// NAVIGATION FUNCTIONALITY: \\

			// func to reset submenus to closed
			function closeNav(runOnce) {
				if (!runOnce) {
					runOnce = false;
				}
				Array.prototype.forEach.call(submenus, function (e) {
					// var but = e.getElementsByTagName('button')[0];
					var list = e.getElementsByTagName('ul')[0];
					if (runOnce && e.classList.contains('active')) {
						list.className = 'open';
					} else {
						list.className = 'closed';
					}
				});
			}
			// sub menu click events init
			Array.prototype.forEach.call(submenus, function (elm) {

				var link = elm.getElementsByTagName('a')[0];
				var list = elm.getElementsByTagName('ul')[0];

				if (eventTrigger === 'hover') {

					elm.addEventListener('mouseover', function () {
						closeNav();
						list.className = 'open';
						elm.classList.add('sub-open');
					});

					elm.addEventListener('mouseout', function () {
						list.className = 'closed';
						elm.classList.remove('sub-open');
					});

				} else {

					link.addEventListener('click', function (e) {
						e.preventDefault();
						if (list.classList.contains('closed')) {
							closeNav();
							list.className = 'open';
							elm.classList.add('sub-open');
						} else {
							list.className = 'closed';
							elm.classList.remove('sub-open');
						}
					});
				}
				closeNav(true);
			});
		}


	},
	// page specific start


	'homepage': {
		init: function () {


		}
	},


	'page_a': {
		init: function () {}
	},


	'page_b': {
		init: function () {}
	}
	// page specific end
};

// The routing fires all common scripts, followed by the page specific scripts.
// Add additional events for more control over timing e.g. a finalize event
var FPM_UTIL = {
	fire: function (func, funcname, args) {
		var fire;
		var namespace = FPM;
		funcname = (funcname === undefined) ? 'init' : funcname;
		fire = func !== '';
		fire = fire && namespace[func];
		fire = fire && typeof namespace[func][funcname] === 'function';

		if (fire) {
			namespace[func][funcname](args);
		}
	},
	loadEvents: function () {
		// Fire common init JS
		FPM_UTIL.fire('common');

		document.body.className.replace(/-/g, '_').split(/\s+/).forEach(function (classnm) {
			FPM_UTIL.fire(classnm);
			FPM_UTIL.fire(classnm, 'finalize');
		});

		// Fire common finalize JS
		FPM_UTIL.fire('common', 'finalize');
	}
};

// Load Events
FPM_UTIL.loadEvents();