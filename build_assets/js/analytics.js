/*
========================================================================
* DOM-based Routing
* Based on http://goo.gl/EUTi53 by Paul Irish
*
* Only fires on body classes that match. If a body class contains a dash,
* replace the dash with an underscore when adding it to the object below.
* ========================================================================
*/

// Use this variable to set up the common and page specific functions. If you
// rename this variable, you will also need to rename the namespace below.

var ANALYTICS = {
	// All pages
	'common': {
		init: function() {

		}
	},
	// page specific start
	'homepage': {
		init: function() {}
	},
	// page specific end
};

// The routing fires all common scripts, followed by the page specific scripts.
// Add additional events for more control over timing e.g. a finalize event
var ANALYTICS_UTIL = {
	fire: function(func, funcname, args) {
		var fire;
		var namespace = ANALYTICS;
		funcname = (funcname === undefined) ? 'init' : funcname;
		fire = func !== '';
		fire = fire && namespace[func];
		fire = fire && typeof namespace[func][funcname] === 'function';

		if (fire) {
			namespace[func][funcname](args);
		}
	},
	loadEvents: function() {
		// Fire common init JS
		ANALYTICS_UTIL.fire('common');

		document.body.className.replace(/-/g, '_').split(/\s+/).forEach(function(classnm) {
			ANALYTICS_UTIL.fire(classnm);
			ANALYTICS_UTIL.fire(classnm, 'finalize');
		});

		// Fire common finalize JS
		ANALYTICS_UTIL.fire('common', 'finalize');
	}
};

// Load Events
ANALYTICS_UTIL.loadEvents();
