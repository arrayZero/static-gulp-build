(function () {
	/*
	 * ========================================================================
	 * Fire an event launching a modal window of content when clicked
	 *
	 * USAGE(html) - Include the following attributes on the trigger element:
	 * class = "modalTrigger"
	 * data-modal = "MODAL-ID-NAME-HERE"
	 * USAGE(JS):
	 * initialize: Modals.init();
	 * ========================================================================
	 */

	Modals.init = function () {
		// Assign Modal vars
		Modals.modalTriggers = Array.from(document.getElementsByClassName('modalTrigger'));
		Modals.modalElements = Array.from(document.getElementsByClassName('modal'));
		// Assign open click event listener
		Modals.modalTriggers.forEach(function (trigger) {
			trigger.addEventListener('click', function (e) {
				e.preventDefault();
				var modalID = this.dataset.modal;
				var modal = document.getElementById(modalID);
				Modals.open(modal);
			});
		});
		// Assign global modal close button click event listeners
		Modals.modalElements.forEach(function (modal) {
			modal.addEventListener('click', function (e) {
				target = e.target;
				// close if has close class
				if (target.classList.contains('close')) {
					Modals.close(modal);
				} // else if(?){ add other target events }
			});
		});
	};

	// Single modal open
	Modals.open = function (modalElm) {
		modalElm.classList.add('is-open');
		modalElm.setAttribute('aria-hidden', 'false');
		document.body.classList.add('lock');
	};

	// Single modal close
	Modals.close = function (modalElm) {
		modalElm.setAttribute('aria-hidden', 'true');
		document.body.classList.remove('lock');
		// finish animation before 'close'
		modalElm.addEventListener('animationend', function handler() {
			modalElm.classList.remove('is-open');
			modalElm.removeEventListener('animationend', handler, false);
		}, false);
	};

	// Modals.closeAll= function () {
	// 	Modals.modalElements.forEach(function (modal) {
	// 	});
	// }
})(window.Modals = window.Modals || {});


(function (EL) {
	/*
	 * ========================================================================
	 * Listen for links that don 't match the current base URL and fire an event when clicked.
	 *
	 * Dependencies: Modals object
	 *
	 * USAGE(html): Auto detects based on URL no html attributes needed
	 * USAGE(JS):
	 * 	whitelist: ExternalLinks.whiteList=['someurl.com', 'anotherurl.com'];
	 * 	initialize: ExternalLinks.init();
	 * ========================================================================
	 */

	EL.whiteList = [];

	EL.init = function () {
		var links = Array.from(document.querySelectorAll('a'));
		var localURL = window.location.href.substr(0, window.location.href.indexOf('/', 10));
		var externalModal = document.getElementById('external');
		var continueLink = externalModal.getElementsByClassName('continue')[0];
		// modal continue link
		continueLink.addEventListener('click', function () {
			Modals.close(externalModal);
		});
		links.forEach(function (link) {
			var href = link.getAttribute('href');
			var external = true;
			// check whitelist first
			if (ExternalLinks.whiteList.length) {
				ExternalLinks.whiteList.forEach(e => {
					if (href.indexOf(e) > -1) {
						external = false;
						link.setAttribute('target', '_blank');
						link.setAttribute('rel', 'noopener');
					}
				});
			}
			// test href for external link
			if (/^(http|https):\/\//i.test(href) && href.indexOf(localURL) && external) {
				link.classList.add('external');
				link.setAttribute('rel', 'noopener');
				// external link click event
				link.addEventListener('click', function (e) {
					e.preventDefault();
					continueLink.setAttribute('href', e.target.href);
					Modals.open(externalModal);
				});
			}
		});
	};

})(window.ExternalLinks = window.ExternalLinks || {});