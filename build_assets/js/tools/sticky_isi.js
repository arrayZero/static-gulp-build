(function () {

		StickyISI.init = function () {
			//Use mobile VH to get proper full height
			StickyISI.mobileVH();
			// set vars

			var jumpLink = document.getElementById('isi_jump'); // scroll to static isi
			var expandLink = document.getElementById('isi_expand_collapse'); // expand fixed isi
			StickyISI.cloneTarget = document.getElementsByClassName('clone_target')[0];
			var stickyContentTarget = document.getElementsByClassName('sticky_content')[0]; // container the cloned content will be appended to
			var staticElement = document.getElementById('isi-container'); // The highest level container of the ISI at the bottom of the page. Not the sticky part
			var stickyElement = document.getElementById('isi_sticky'); // The top level container of the sticky section
			var scroller = StickyISI.cloneTarget.cloneNode(true);
			// CLONE SET UP \\
			scroller.classList.remove('clone_target');
			scroller.removeAttribute('id');
			stickyContentTarget.appendChild(scroller);
			stickyElement.setAttribute('style', 'bottom:0');
			//run initial show/hide
			StickyISI.hideShow(stickyElement, staticElement);


			// EVENT LISTENERS
			expandLink.addEventListener('click', function (e) {
				StickyISI.expandCollapse(e);
			});

			jumpLink.addEventListener('click', function () {
				// requires scrollto.js
				Tools.scrollToElm('#isi-container');
			});

			window.addEventListener('scroll', function () {
				StickyISI.hideShow(stickyElement, staticElement);
			});

			window.addEventListener('resize', function () {
				StickyISI.hideShow(stickyElement, staticElement);
			});
		};


		/*
		 * ========================================================================
		 * hide/show sticky isi when scrolled to static scroll position
		 * arguments: element(static version of isi) & element(sticky version of isi)
		 * ========================================================================
		 */
		StickyISI.hideShow = function (stickyElement, staticElement) {
			var isiScrollContainerSelector = document.getElementsByClassName('isi-wrapper')[0]; // The section that should scroll while the footer is stuck
			var clonedElement = stickyElement;
			if (Tools.offset(stickyElement).top > Tools.offset(staticElement).top) {
				clonedElement.classList.remove('show');
			} else {
				if (!clonedElement.classList.contains('show')) {
					isiScrollContainerSelector.scrollTop = 0;
				}
				clonedElement.classList.add('show');
				StickyISI.cloneTarget.classList.remove('expanded');
			}
		};

		/*
		 * ========================================================================
		 * Scrolls to static ISI position
		 * arguments: element(static version of isi)
		 * TODO: abstratct out into reusable scroll to funtionality
		 * ========================================================================
		 */
		StickyISI.scrollToISI = function (staticElement) {
			var docPosition = window.pageYOffset || document.documentElement.scrollTop;
			var isiDistance = staticElement.offsetTop - docPosition;
			var counter = 0;
			//smooth scroll mathmatical formula based on Smoothstep
			function smoothScroll(n) {
				return n * n * (3 - 2 * n);
			}
			// Scroll via interval
			var scrollInterval = setInterval(function () {
					scrollRoot.scrollTop = docPosition + isiDistance * smoothScroll(counter++/ 120);
						if (counter > 120) {
							clearInterval(scrollInterval);
						}
					}, 10);
			};


			/*
			 * ========================================================================
			 * expand/collapse isi functioonality
			 * arguments: event
			 * ========================================================================
			 */
			StickyISI.expandCollapse = function (e) {
				var stuckisi = document.getElementById('isi_sticky');
				var icon = e.target;
				if (stuckisi.classList.contains('expanded')) {
					stuckisi.classList.add('minimized');
					stuckisi.classList.remove('expanded');
					icon.classList.remove('fa-angle-down');
					icon.classList.add('fa-angle-up');
				} else if (stuckisi.classList.contains('minimized')) {
					stuckisi.classList.remove('minimized');
					icon.classList.remove('fa-angle-down');
					icon.classList.add('fa-angle-up');
				} else {
					stuckisi.classList.add('expanded');
					icon.classList.remove('fa-angle-up');
					icon.classList.add('fa-angle-down');
				}
			};


			/*
			 * ========================================================================
			 * Mobile browser 100vh isi fix
			 * Uses css variables
			 * CSS should have fallback
			 * USAGE(css):
			 * height: 100 vh;
			 * height: calc(var (--vh, 1 vh) * 100);
			 * ========================================================================
			 */
			StickyISI.mobileVH = function () {
				// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
				let vh = window.innerHeight * 0.01;
				// Then we set the value in the --vh custom property to the root of the document
				document.documentElement.style.setProperty('--vh', `${vh}px`);
				window.addEventListener('resize', () => {
					let vh = window.innerHeight * 0.01;
					document.documentElement.style.setProperty('--vh', `${vh}px`);
				});
			};

		})(window.StickyISI = window.StickyISI || {});