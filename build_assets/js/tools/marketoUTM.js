(function (mutm) {
	//set this to false to use cookie values if they exist (if false, will not check querystring first).
	mutm.ppcUseLatestValues = true;
	// true = use querystring values if they exist then attempt to get cookie values
	// false = use cookie values if they exist

	// function to grab params from cookie
	mutm.getCookie = function (paramName) {
		var i, x, y, cookie = document.cookie.split(';');
		for (i = 0; i < cookie.length; i++) {
			x = cookie[i].substr(0, cookie[i].indexOf('='));
			y = cookie[i].substr(cookie[i].indexOf('=') + 1);
			x = x.replace(/^\s+|\s+$/g, '');
			if (x === paramName) {
				return unescape(y);
			}
		}
	};

	// function to create cookie
	mutm.setCookie = function (paramName, value, exdays) {
		var exdate = new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var baseURL = window.location.hostname;
		baseURL = baseURL.replace('www.', '');
		var cookieValue = escape(value) + ((exdays == null) ? '' : '; domain=' + baseURL + '; path=/; expires=' + exdate.toUTCString());
		document.cookie = paramName + '=' + cookieValue;
	};

	// function to check if cookie exists and, if so, run the setCookie function
	mutm.checkCookie = function (paramName, paramURLName) {
		var paramValue = mutm.getCookie(paramName);
		if ((paramValue != null && paramValue !== '' && paramValue !== 'undefined') && mutm.ppcUseLatestValues === false) {
			//this means the param name/value pair exists - and we don't want to use latest
		} else {
			//this means the param name/value pair does not exist - so create it
			//grab values from URL
			var pageURL = window.location.search.substring(1);
			var URLVariables = pageURL.split('&');
			for (var i = 0; i < URLVariables.length; i++) {
				var parameterName = URLVariables[i].split('=');
				if (parameterName[0] === paramURLName) {
					//filter out '#' in case that is in the last URL param
					paramValue = parameterName[1].split('#')[0];
				}
			}

			if (paramValue !== 'undefined' && paramValue !== '' && paramValue !== null) {
				//create cookie
				mutm.setCookie(paramName, paramValue, 365);
			}
		}
	};

	//function to setup the parameters and save the cookie values
	mutm.ppcUrlCookiePart1 = function () {
		//setup list/array of parameters desired. names on right should match querystring names
		var paramNames = new Array(
			'ppcSource;utm_source',
			'ppcMedium;utm_medium',
			'ppcCampaign;utm_campaign',
			'ppcAdGroup;utm_adgroup',
			'ppcKeyword;utm_term',
			'ppcContent;utm_content'
		);

		//loop through all params and create cookie
		for (i = 0; i < paramNames.length; i++) {
			var paramObject = paramNames[i].split(';'); //split out the cookie name and url name
			var paramName = paramObject[0];
			var paramURLName = paramObject[1];
			//start the cookie creation
			mutm.checkCookie(paramName, paramURLName);
		}
	};

	//function to grab cookie params
	mutm.mGetCookie = function (paramName) {
		var i, x, y, cookie = document.cookie.split(';');
		for (i = 0; i < cookie.length; i++) {
			x = cookie[i].substr(0, cookie[i].indexOf('='));
			y = cookie[i].substr(cookie[i].indexOf('=') + 1);
			x = x.replace(/^\s+|\s+$/g, '');
			if (x === paramName) {
				return unescape(y);
			}
		}
	};

	//function to check if cookie exists and, if so, fill out the corresponding form fields
	mutm.mCheckCookie = function (paramName, paramFieldName) {
		var paramValue = mutm.mGetCookie(paramName);
		if (paramValue != null && paramValue !== '' && paramValue !== 'undefined') {
			try {
				var obj1 = document.getElementsByName(paramFieldName);
				obj1[0].value = paramValue;
				return true;
			} catch (err) {
				return false;
			}
		}
		return false;
	};

	//function to setup parameters and begin cookie value insertion into marketo form
	mutm.ppcUrlCookiePart2 = function () {
		//setup list/array of parameters desired. names on right should match hidden form field names
		var paramNames = new Array(
			'ppcSource;utm_source__c',
			'ppcMedium;utm_medium__c',
			'ppcCampaign;utm_campaign__c',
			'ppcAdGroup;utm_adgroup__c',
			'ppcKeyword;utm_term__c',
			'ppcContent;utm_content__c'
		);

		//loop through all params and create cookie
		for (i = 0; i < paramNames.length; i++) {
			var paramObject = paramNames[i].split(';'); //split out the cookie name and url name
			var paramName = paramObject[0];
			var paramFieldName = paramObject[1];
			//start the cookie creation
			mutm.mCheckCookie(paramName, paramFieldName);
		}
	}

	//ppcUrlCookiePart1 will grab values from the querystring and save them in cookies
	mutm.ppcUrlCookiePart1();

	//ppcUrlCookiePart2 will retrive values from the cookies and populate the hidden form fields - should be in the onload
	try {
		//attempt for Marketo form
		MktoForms2.whenReady(function (form) {
			mutm.ppcUrlCookiePart2();
		});
	} catch (err) {
		//if error on Marketo form, try loading for regular form.
		mutm.ppcUrlCookiePart2();
	}
})(window.marketoUTM = window.marketoUTM || {});