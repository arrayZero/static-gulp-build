(function () {
	/*
	 * ========================================================================
	 * returns the document position of an element
	 * ========================================================================
	 */
	Tools.offset = function (e) {
		var rect = e.getBoundingClientRect(),
			scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
			scrollTop = window.pageYOffset || document.documentElement.scrollTop;
		return {
			top: rect.top + scrollTop,
			left: rect.left + scrollLeft
		};
	};

})(window.Tools = window.Tools || {});