(function () {
		/*
		 * ========================================================================
		 * Sets scrollRoot as browser appropriate scroll variable
		 * ========================================================================
		 */
		setScrollRoot = function () {
			var html = document.documentElement,
				body = document.body,
				cacheTop = ((typeof window.pageYOffset !== 'undefined') ? window.pageYOffset : null) || body.scrollTop || html.scrollTop, // cache the window's current scroll position
				root;
			html.scrollTop = body.scrollTop = cacheTop + (cacheTop > 0) ? -1 : 1;
			// find root by checking which scrollTop has a value larger than the cache.
			root = (html.scrollTop !== cacheTop) ? html : body;
			// restore the window's scroll position to cached value
			root.scrollTop = cacheTop;
			// set scrollRoot to scrolling root element
			return root;
		};

		window.scrollRoot = setScrollRoot();

		/*
		 * ========================================================================
		 * Scroll into view functionality
		 * function lives under tools object for easy access/name spacing
		 * ========================================================================
		 */
		Tools.scrollToElm = function (elm) {
			// console.log(elm);
			var scrollElm = document.querySelectorAll(elm)[0];
			// console.log(scrollElm);
			var docPosition = window.pageYOffset || document.documentElement.scrollTop;
			var isiDistance = scrollElm.offsetTop - docPosition;

			var counter = 0;
			//smooth scroll mathmatical formula based on Smoothstep
			function smoothScroll(n) {
				return n * n * (3 - 2 * n);
			}
			// Scroll via interval
			var scrollInterval = setInterval(function () {
					scrollRoot.scrollTop = docPosition + isiDistance * smoothScroll(counter++/ 120);
						if (counter > 120) {
							clearInterval(scrollInterval);
						}
					}, 5);
			};


			/*
			 * ========================================================================
			 * assigns scroll into view  function to all jumplinks
			 * ========================================================================
			 */
			document.querySelectorAll('a[href^="#"]').forEach(anchor => {
				anchor.addEventListener('click', function (e) {
					e.preventDefault();
					Tools.scrollToElm(this.getAttribute('href'));
				});
			});

			// TODO: enable CSS scroll-behavior and/or JS scrollIntoView() as compatability improves

			// // check to see if the browser supports CSS smooth scroll
			// if (CSS.supports('scroll-behavior', 'smooth') && document.body.scrollIntoView) {
			// 	document.body.style.scrollBehavior = 'smooth';

			// } else {
			// 	document.querySelectorAll('a[href^="#"]').forEach(anchor => {
			// 		anchor.addEventListener('click', function (e) {
			// 			e.preventDefault();
			// 			document.querySelector(this.getAttribute('href')).scrollIntoView({
			// 				behavior: 'smooth'
			// 			});
			// 		});
			// 	});
			// }
		}());