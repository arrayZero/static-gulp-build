/*
 * ========================================================================
 * Simple collapse & expand functionality
 *
 * ARGUMENTS:
 * collapseOnOpen - accordian like functionality
 * openFirst - keeps first expand area open on page load
 *
 * USAGE:
 * <div class="expand"><div class="question" >...< /div><div class="answer">...</div></div>
 * first-child gets click event
 * last-child is expandable content with a .hide class on the parent(.expand)
 * ========================================================================
 */
(function () {
	Expand.init = function (options) {
		var defaults = {
			elementGroup: 'expand',
			collapseOnOpen: false,
			openFirst: true
		};
		var expand = document.getElementsByClassName(options.elementGroup);
		options = Object.assign({}, defaults, options);
		Array.prototype.forEach.call(expand, function (e, i) {
			var trigger = e.firstChild;
			if (options.openFirst && i >= 1) {
				e.classList.add('hide');
			} else {
				e.classList.add('show');
			}

			trigger.addEventListener('click', function () {
				if (e.classList.contains('show')) {
					e.classList.remove('show');
					e.classList.add('hide');
				} else {
					if (options.collapseOnOpen) {
						Array.prototype.forEach.call(expand, function (e) {
							e.classList.remove('show');
							e.classList.add('hide');
						});
					}
					e.classList.remove('hide');
					e.classList.add('show');
				}
			});
		});
	};

})(window.Expand = window.Expand || {});