const gulp = require('gulp');
const reqDir = require('require-dir')('./gulp/');
global.browserSync = require('browser-sync').create();

gulp.task('default',
	gulp.series('info', 'clean', 'copyAssets', 'apache', 'css', 'modernizr', 'JavaScript', 'pug')
);


gulp.task('dev',
	gulp.series('default', 'watch')
);